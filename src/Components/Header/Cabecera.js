import React from "react";
import "../Header/cabecera.css";
import imgb from "../../images/cuadro.png";

const Cabecera = () => {
  return (
    <div className="go-container">
      <p>Gmail</p> &nbsp;&nbsp;
      <p>Imagenes</p> &nbsp;&nbsp;
      <img src={imgb} alt="cuadro" className="imgb" />
    </div>
  );
};

export default Cabecera;
