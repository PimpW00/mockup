import React from "react";
import imga from "../../images/logo.png";
import lup from "../../images/lupa.png";
import mic from "../../images/mic.png";
import "../Input/entrada.css";

const Entrada = () => {
  return (
    <div className="imga">
      <img src={imga} />
      <img className="lup" src={lup} />
      <img className="mic" src={mic} />
      <input className="id"></input>
    </div>
  );
};

export default Entrada;
