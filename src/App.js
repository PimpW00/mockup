import React from "react";
import Cabecera from "./Components/Header/Cabecera";
import Entrada from "./Components/Input/Entrada";
import Press from "./Components/button/Press";
import Foot from "./Components/Footer/Foot";

function App() {
  return (
    <div>
      <Cabecera />
      <main>
        <Entrada />
        <Press />
      </main>
      <Foot />
    </div>
  );
}

export default App;
